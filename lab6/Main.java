package shahryar.main;

import shahryar.shapes.Circle;

public class Main {
	
		public static void main (String[] args)
		 {
			ArrayList<Circle> circles = new ArrayList<Circle>();
			
			Circle circle = new Circle(5);
			
			circles.add(circle);
			circles.add(new Circle(4));
			circles.add(new Circle(6));
			
			for(int i = 0; i < circle.size(); i++) 
			{
				System.out.println(circles.get(i).area());
			}

		}

}
